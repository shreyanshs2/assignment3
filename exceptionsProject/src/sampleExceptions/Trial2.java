package sampleExceptions;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class Trial2 {
    public static void main(String[] args) {
        try {
            int x = 10, y= 0, z;
            z = x/y;
            int[] arr1 = new int[5];
            arr1[5] = 6;
            FileReader fr = new FileReader("file1.txt");
        } catch (ArithmeticException | ArrayIndexOutOfBoundsException| FileNotFoundException e) { // runtime exception, unchecked exception
            // instanceof or print error objects message
            System.out.println("Exception");
        }
        // compile time exceptions need to be in a try catch block that can handle the exeption
    }
    public static void method1() throws FileNotFoundException{
        
    }
}
