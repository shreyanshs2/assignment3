package sampleExceptions;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class Trial1{
    public static void main(String[] args) {
        try {
            int x = 10, y= 0, z;
            z = x/y;
            int[] arr1 = new int[5];
            arr1[5] = 6;
            FileReader fr = new FileReader("file1.txt");
        } catch (ArithmeticException e) { // runtime exception, unchecked exception
            System.out.println("AE");
        } catch (ArrayIndexOutOfBoundsException e){ // runtime exception, unchecked exception
            System.out.println("Out of B");
        } catch (FileNotFoundException e){ // compile time exception, checked exception
            System.out.println("file not found");
        }

    }
}