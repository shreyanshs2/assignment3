package creditCard;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        String creditCard;
        Scanner sc = new Scanner(System.in);
        creditCard = sc.nextLine();
        try{
            isValidCreditCard(creditCard);
        }
        catch(creditCardInvalidException e){
            System.out.println(e.toString());
        }
        sc.close();
    }

    public static boolean isValidCreditCard(String cardNumber) throws creditCardInvalidException {
        if (cardNumber.length() != 16) {
            throw new creditCardInvalidException("Card number length invalid ");
        }
        for (char c : cardNumber.toCharArray()) {
            if (!Character.isDigit(c))
                throw new creditCardInvalidException("Only digits allowed");
        }

        return true;
    }

}
